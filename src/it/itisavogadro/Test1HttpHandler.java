package it.itisavogadro;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;

public class Test1HttpHandler implements HttpHandler {
	@Override
	public void handle(HttpExchange httpExchange) throws IOException {
		System.out.println("HTTP request TEST-1");

		System.out.println(
				"\nURI:"+httpExchange.getRequestURI()+
				"\nmethod:"+httpExchange.getRequestMethod()+
				"\ncontext-path:"+httpExchange.getHttpContext().getPath()+
				"\npath:"+httpExchange.getRequestURI().getPath()+
				"\nquery:"+httpExchange.getRequestURI().getQuery()+		// percorso?parametro1=valore1&parametro2=val2
				""
		);

		String htmlResponse = ""+
			"<html>"+
			"<body>"+
			"<h1>"+httpExchange.getRequestMethod()+" - Hello Test-1 App</h1>"+
			"<p>Per eseguire il login clicca <a href=\"/login\">QUI</a></p>"+
			"</body>"+
			"</html>"
		;
		Utilities.flushHtml(httpExchange,htmlResponse);
	}
}
