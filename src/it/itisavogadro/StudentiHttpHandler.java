package it.itisavogadro;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;

public class StudentiHttpHandler implements HttpHandler {
	@Override
	public void handle(HttpExchange httpExchange) throws IOException {
		System.out.println("HTTP request STUDENTI");

		if (!Utilities.isAuthenticated(httpExchange)) {
			Headers responseHeaders = httpExchange.getResponseHeaders();
			responseHeaders.set("Location", "/static/login.html");
			httpExchange.sendResponseHeaders(302, 0);
			return;
		}

		// Context "/studenti"
		// quello che segue il prefisso di context lo uso come selettore di azioni da eseguire
		String azione = httpExchange.getRequestURI().getPath().replace("/studenti/","");

		String htmlResponse;
		if ("GET".equalsIgnoreCase(httpExchange.getRequestMethod())) {
			switch(azione) {
				case "elenco":
					elencoStudenti(httpExchange);
					break;
				case "nuovo":
					aggiungiStudente(httpExchange);
					break;
				case "modifica":
					modificaStudente(httpExchange);
					break;
				case "cancella":
					cancellaStudente(httpExchange);
					break;
				default:
					htmlResponse = "" +
						"<html>" +
						"<body>" +
						"<h1>OPERAZIONE "+azione+" NON E' GESTITA</h1>" +
						"</body>" +
						"</html>"
					;
					Utilities.flushHtml(httpExchange,htmlResponse);
					return;
			}
			return;
		}
		htmlResponse = "" +
				"<html>" +
				"<body>" +
				"<h1>" + httpExchange.getRequestMethod() + " - OPERAZIONE CON METHOD NON GESTITO</h1>" +
				"</body>" +
				"</html>"
		;
		Utilities.flushHtml(httpExchange,htmlResponse);
	}

	private void cancellaStudente(HttpExchange httpExchange) throws IOException {
		String htmlResponse = "" +
			"<html>" +
			"<body>" +
			"<h1>" + httpExchange.getRequestMethod() + " - CANCELLAZIONE STUDENTE</h1>" +
			"</body>" +
			"</html>"
		;
		Utilities.flushHtml(httpExchange,htmlResponse);
	}

	private void modificaStudente(HttpExchange httpExchange) throws IOException {
		String htmlResponse = "" +
			"<html>" +
			"<body>" +
			"<h1>" + httpExchange.getRequestMethod() + " - MODIFICA STUDENTE</h1>" +
			"</body>" +
			"</html>"
		;
		Utilities.flushHtml(httpExchange,htmlResponse);
	}

	private void aggiungiStudente(HttpExchange httpExchange) throws IOException {
		String htmlResponse = "" +
			"<html>" +
			"<body>" +
			"<h1>" + httpExchange.getRequestMethod() + " - AGGIUNTA NUOVO STUDENTE</h1>" +
			"</body>" +
			"</html>"
		;
		Utilities.flushHtml(httpExchange,htmlResponse);
	}

	private void elencoStudenti(HttpExchange httpExchange) throws IOException {
		String htmlResponse = "" +
			"<html>" +
			"<body>" +
			"<h1>" + httpExchange.getRequestMethod() + " - ELENCO DI TUTTI GLI STUDENTI</h1>" +
			"</body>" +
			"</html>"
		;
		Utilities.flushHtml(httpExchange,htmlResponse);
	}
}
