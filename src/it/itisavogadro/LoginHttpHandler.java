package it.itisavogadro;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;

public class LoginHttpHandler implements HttpHandler {
	@Override
	public void handle(HttpExchange httpExchange) throws IOException {
		System.out.println("HTTP request LOGIN");

		System.out.println(
			"\nURI:"+httpExchange.getRequestURI()+
			"\nmethod:"+httpExchange.getRequestMethod()+
			"\ncontext-path:"+httpExchange.getHttpContext().getPath()+
			"\npath:"+httpExchange.getRequestURI().getPath()+
			"\nquery:"+httpExchange.getRequestURI().getQuery()+		// percorso?parametro1=valore1&parametro2=val2
			""
		);
		String htmlResponse = "";
		if (!"POST".equalsIgnoreCase(httpExchange.getRequestMethod())) {
			// GET, PUT, DEL non gestiti
			htmlResponse = "" +
					"<html>" +
					"<body>" +
					"<h1>" + httpExchange.getRequestMethod() + " - OPERAZIONE CON METHOD NON GESTITO</h1>" +
					"</body>" +
					"</html>"
			;
			Utilities.flushHtml(httpExchange,htmlResponse);
			return;
		}
		// per la login useremo solo la POST
		Scanner scanner = new Scanner(httpExchange.getRequestBody(), StandardCharsets.UTF_8.name());
		String paramLine = scanner.useDelimiter("\\A").next();
		String params[] = paramLine.split("&");
		String user = "missing";
		String pass = "missing";
		for (String param : params) {
			String paramName  = param.split("=")[0];
			String paramValue = param.split("=")[1];
			paramValue = java.net.URLDecoder.decode(paramValue, StandardCharsets.UTF_8.name());;
			if ("user".equalsIgnoreCase(paramName)) {
				user = paramValue;
			}
			if ("password".equalsIgnoreCase(paramName)) {
				pass = paramValue;
			}
		}
		// ACCEDERE AL DB PER VERIFICARE
		boolean credenzialiValide = false;
		try {
		//	String dbURL = "jdbc:derby://localhost:1527/avogadro;create=true";
			String dbURL = "jdbc:postgresql://localhost:5432/avogadro";
			Connection conn = DriverManager.getConnection(dbURL, "postgres", "pippo");
			Statement stmt = (Statement) conn.createStatement();

			ResultSet rs = stmt.executeQuery(
				"select * from webapp.utente" // where username = '"+user+"'"
			);

			String utenti = "<ul>";

			MessageDigest md = MessageDigest.getInstance("SHA-1");
			String sha1 = String.format("%040x", new BigInteger(1, md.digest(pass.getBytes())));

			while (rs.next()) {
				Utente utente = new Utente();
				utente.setId(rs.getInt("id"));
				utente.setUsername(rs.getString("username"));
				utente.setPassword(rs.getString("password"));
				utente.setEmail(rs.getString("email"));
				utente.setCognome(rs.getString("cognome"));
				utente.setNome(rs.getString("nome"));

				if (utente.getUsername().equalsIgnoreCase(user)) {
					if (utente.getPassword().equals(sha1)) {
						credenzialiValide = true;
					}
				}

				System.out.println("Utente:"+utente);
				utenti += "<li>"+utente.toString("<br>")+"</li>";
			}
			rs.close();

			if (credenzialiValide) {
				String token = "12345";
				httpExchange.getResponseHeaders().set("Set-Cookie","AUTH_TOKEN="+token+"; path=/; Max-Age=60");
			} else {
				httpExchange.getResponseHeaders().set("Set-Cookie","AUTH_TOKEN=; Max-Age=0");
			}

			htmlResponse = ""+
					"<html>" +
					"<body>" +
					"<h1>" + httpExchange.getRequestMethod() + " - Eseguito Login Test-1 App con le credenziali:</h1>" +
					"user:"+user+"<br>"+
					"pass:"+pass+"<br>"+
					"sha1:"+sha1+"<br>"+
					(credenzialiValide ? "<h1>ACCESSO CONSENTITO</h1>" : "<h1 style='color:red'>ACCESSO NEGATO</h1>")+
					"<a href=\"/static/menu.html\">Vai al menù</a><br>"+
					"utenti:"+utenti+
					"</body>" +
					"</html>"
			;
		} catch (Exception ex) {
			ex.printStackTrace();
			htmlResponse = "" +
					"<html>" +
					"<body>" +
					"<h1>Error: "+ex.getMessage()+"</h1>" +
					"</body>" +
					"</html>"
			;
		}
		Utilities.flushHtml(httpExchange,htmlResponse);
	}
}
