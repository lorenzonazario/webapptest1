package it.itisavogadro;

import com.sun.net.httpserver.HttpExchange;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;


public class Utilities {

	public static void flushHtml(HttpExchange httpExchange, String htmlResponse) throws IOException {
		String encoding = "UTF-8";
		httpExchange.getResponseHeaders().set("Content-Type", "text/html; charset=" + encoding);
		httpExchange.sendResponseHeaders(200, 0);
		OutputStream outputStream = httpExchange.getResponseBody();
		outputStream.write(htmlResponse.getBytes());
		outputStream.flush();
		outputStream.close();
	}

	public static boolean isAuthenticated(HttpExchange httpExchange) {
		try {
			List<String> cookies = httpExchange.getRequestHeaders().get("Cookie");
			for (String cookie : cookies) {
				String cookieSplit[] = cookie.split("=");
				if (cookieSplit[0].equalsIgnoreCase("AUTH_TOKEN")) {
					return true;
				}
			}
			// non c'è il cookie AUTH_TOKEN quindi deve autenticarsi
			throw new RuntimeException("Not authenticated");
		} catch (Exception ex) {
			return false;
		}
	}
}
