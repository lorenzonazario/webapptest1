package it.itisavogadro;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class StaticHttpHandler implements HttpHandler {
	@Override
	public void handle(HttpExchange httpExchange) throws IOException {
		System.out.println("HTTP request Static Context");

		String htmlResponse;

		try {
			System.out.println("\nURI:" + httpExchange.getRequestURI()+
				"\nmethod:" + httpExchange.getRequestMethod() +
				"\ncontext-path:" + httpExchange.getHttpContext().getPath() +
				"\npath:" + httpExchange.getRequestURI().getPath() +
				"\nquery:" + httpExchange.getRequestURI().getQuery() +        // percorso?parametro1=valore1&parametro2=val2
				""
			);

			// Context "/static"
			// il path web, se inizia con static, lo usiamo come path di file risorsa statico

			if (!"GET".equalsIgnoreCase(httpExchange.getRequestMethod())) {
				htmlResponse = "" +
					"<html>" +
					"<body>" +
					"<h1>" + httpExchange.getRequestMethod() + " - OPERAZIONE CON METHOD NON GESTITO</h1>" +
					"</body>" +
					"</html>"
				;
				Utilities.flushHtml(httpExchange,htmlResponse);
				return;
			}
			// I file static DEVONO essere richiesti tramite GET

			// cerchiamo se esiste un file con pathname uguale al path web
			String staticFilePath = httpExchange.getRequestURI().getPath();

			//   "/static/nomefile"
			switch (staticFilePath) {
				// elenco pagine statiche che necessitano della autenticazione:
				case "/static/menu.html":
					// verifica di autenticazione
					if (!Utilities.isAuthenticated(httpExchange)) {
						// errore non autenticato
						throw new Exception("Pagina riservata, richiede l'autenticazione");
					}
					break;
			}

			URL resourceUrl = this.getClass().getResource(staticFilePath);
			if (resourceUrl == null) {
				throw new RuntimeException("pagina mancante");
			}
			Path path = Paths.get(resourceUrl.toURI());
			htmlResponse = new String(Files.readAllBytes(path));

			if (staticFilePath.toLowerCase().endsWith(".html")) {
				httpExchange.getResponseHeaders().set("Content-Type", "text/html; charset=UTF-8");
			}
			httpExchange.sendResponseHeaders(200,0);

			OutputStream outputStream = httpExchange.getResponseBody();
			outputStream.write(htmlResponse.getBytes());
			outputStream.flush();
			outputStream.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			htmlResponse = "" +
				"<html>" +
				"<body>" +
				"<h1>ERRORE:</h1>" +
				"<h1>"+ex.getMessage()+"</h1>" +
				"</body>" +
				"</html>"
			;
			Utilities.flushHtml(httpExchange,htmlResponse);
		}
	}
}
