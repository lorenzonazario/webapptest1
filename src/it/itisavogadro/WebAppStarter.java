package it.itisavogadro;

import com.sun.net.httpserver.HttpServer;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class WebAppStarter {
	public static void main(String[] args) {
		int port = 9999;
		try {
			HttpServer server = HttpServer.create(new InetSocketAddress("localhost", port), 0);

			// contesti NON autenticati
			server.createContext("/", new MainHttpHandler());
			server.createContext("/test-1", new Test1HttpHandler());
			server.createContext("/login",  new LoginHttpHandler());

			// contesti accessibili solo se autenticati
			server.createContext("/static", new StaticHttpHandler());
			server.createContext("/studenti", new StudentiHttpHandler());
//			server.createContext("/docenti", new StudentiHttpHandler());
//			server.createContext("/cambiopassword", new CambioPasswordHttpHandler());

			ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);

			server.setExecutor(threadPoolExecutor);

			server.start();

			System.out.println(" Server started on port "+port);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
