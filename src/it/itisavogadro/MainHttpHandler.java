package it.itisavogadro;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;

public class MainHttpHandler implements HttpHandler {
	@Override
	public void handle(HttpExchange httpExchange) throws IOException {
		System.out.println("HTTP request TEST-1");

		System.out.println(
				"\nURI:"+httpExchange.getRequestURI()+
				"\nmethod:"+httpExchange.getRequestMethod()+
				"\ncontext-path:"+httpExchange.getHttpContext().getPath()+
				"\npath:"+httpExchange.getRequestURI().getPath()+
				"\nquery:"+httpExchange.getRequestURI().getQuery()+		// percorso?parametro1=valore1&parametro2=val2
				""
		);

		if (true) {
			Headers responseHeaders = httpExchange.getResponseHeaders();
			responseHeaders.set("Location", "/static/error.html");
			httpExchange.sendResponseHeaders(302, 0);
			return;
		}

		String htmlResponse = "<!DOCTYPE html>\n" +
			"<html lang=\"en\">\n" +
			"<head>\n" +
			"<meta charset=\"UTF-8\">\n" +
			"<title>WebApp Gestione scuola</title>\n" +
			"<link rel=\"stylesheet\" type=\"text/css\" href=\"scuola.css\">\n" +
			"</head>\n" +
			"<body>\n" +
			"<h1>WebApp Gestione scuola</h1>\n" +
			"<h2>Nell'applicazione questa pagina non è presente</h2>\n" +
			"<p>Per eseguire il login clicca <a href=\"/static/login\">QUI</a></p>"+
			"</body>"+
			"</html>"
		;

		Utilities.flushHtml(httpExchange,htmlResponse);
	}
}
